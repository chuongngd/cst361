package controllers;

import org.slf4j.LoggerFactory;


import beans.Dates;


import org.slf4j.Logger;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.interceptor.Interceptors;

import business.WeatherBusinessInterface;
import util.LoggingInterceptor;

/*
 * this class is weather controller
 */
@Named 
@ManagedBean
@ViewScoped
@Interceptors(LoggingInterceptor.class) 
public class WeatherController implements Serializable{
	/**
	 * 
	 */
	Logger logger = LoggerFactory.getLogger(WeatherController.class);
	private static final long serialVersionUID = 1L;
	@Inject
	WeatherBusinessInterface weatherBusiness;
	
	
	/*
	 * function show weather in a range of time in tabular view
	 */
	public String showWeatherFromTo() {
	
		logger.info("Entering show weather table");
		FacesContext context = FacesContext.getCurrentInstance();
		Dates dates = context.getApplication().evaluateExpressionGet(context, "#{dates}", Dates.class);
		//List<Weather> weathers = weatherBusiness.findWeatherByRange(dates.dateFrom, dates.dateTo);
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("dates",
				dates);
		return "ShowWeatherTable.xhtml";
	}
	
	
	/*
	 * function get the range of dates and return view of charts 
	 */
	public String showWeatherFromToByChart() {
		
		logger.info("Entering show weather table");
		FacesContext context = FacesContext.getCurrentInstance();
		Dates dates = context.getApplication().evaluateExpressionGet(context, "#{dates}", Dates.class);
		//List<Weather> weathers = weatherBusiness.findWeatherByRange(dates.dateFrom, dates.dateTo);
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("dates",
				dates);
		return "ShowCharts.xhtml";
	}
	/*
	public String getWeather() {
		logger.info("Entering weather controller get weather");
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("weathers",
				weatherBusiness.getWeather());
		return "ShowWeather.xhtml";
	}*/
	public WeatherBusinessInterface getWeatherBusiness() {
		return weatherBusiness;
	}
}
