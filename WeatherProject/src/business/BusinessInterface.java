package business;

import javax.ejb.Local;
import beans.User;

/**
 * This interface class is the Business Service that implements the business methods to support the User model
 * 
 */
@Local
public interface BusinessInterface {
	public boolean Authenticate(User t);
	public boolean create(User t);
	public boolean findUser(String username);
	public User findByUser (String username);
}
