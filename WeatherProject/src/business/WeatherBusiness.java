package business;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;


import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import beans.Weather;
import data.WeatherDAOInterface;
import util.LoggingInterceptor;

/*
 *  This class is the Business Service that implements the business methods to support the Weather IoT.
 */
@Interceptors(LoggingInterceptor.class) 
@Stateless
@Local
@LocalBean
public class WeatherBusiness implements WeatherBusinessInterface{
	Logger logger = LoggerFactory.getLogger(UserBusiness.class);
	@EJB
	WeatherDAOInterface<Weather> dao;
	
	
	/*
	 * (non-Javadoc)
	 * @see business.WeatherBusinessInterface#create(beans.Weather)
	 * business function to call dao to insert new weather information
	 * @param model Weather populated with data to save.
     * @return True if OK else False if Error.
	 */
	@Override
	public boolean create(Weather t) {
		// TODO Auto-generated method stub
		logger.info("Entering create weather");
		return dao.insert(t);
	}

	
	/*
	 * (non-Javadoc)
	 * @see business.WeatherBusinessInterface#getWeather()
	 *business function to call dao to get all weather information from database 
	 *
	 */
	@Override
	public List<Weather> getWeather() {
		// TODO Auto-generated method stub
		logger.info("Entering retrieve weather");
		return dao.getWeather();
	}	
	
	/*
	 * (non-Javadoc)
	 * @see business.WeatherBusinessInterface#findWeatherByRange(java.lang.String, java.lang.String)
	 * business function to call dao to retrieve weather data in a range of dates
     * @param from is Beginning DateTime to retrieve.
     * @param to is End DateTime to retrieve.
     * @return dao.findByDateRange(from,to)
	 */
	
	@Override
	public List<Weather> findWeatherByRange(String from, String to){
		return dao.findByDateRange(from, to);
	}

}
