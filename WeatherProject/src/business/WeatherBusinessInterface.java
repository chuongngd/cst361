package business;


import java.util.List;

import javax.ejb.Local;

import beans.Weather;


/**
 * This interface class is the Business Service that implements the business methods to support the Weather IoT.
 * 
 */
@Local
public interface WeatherBusinessInterface {
	public boolean create(Weather t);
	public List<Weather> getWeather();
	public List<Weather> findWeatherByRange(String from, String to);
}
