package business;


import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import beans.User;
import data.DaoInterface;
import util.LoggingInterceptor;


/*
 *  This class is the Business Service that implements the business methods to support the User.
 */
@Interceptors(LoggingInterceptor.class)
@Stateless
@Local
@LocalBean
public class UserBusiness implements BusinessInterface {
	
	Logger logger = LoggerFactory.getLogger(UserBusiness.class);

	@EJB
	DaoInterface<User> Dao;
	public UserBusiness() {
	}
	
	/*
	 * (non-Javadoc)
	 * @see business.BusinessInterface#Authenticate(beans.User)
	 * business function to check if a user is in database
	 * @param user is the input user
	 */
	@Override
	public boolean Authenticate(User user)
	{
		logger.info("Entering Authennticate user");
		//UserDao UserDao = new UserDao();
		return Dao.findBy(user);
	}

	
	/*
	 * (non-Javadoc)
	 * @see business.BusinessInterface#create(beans.User)
	 * business function to call dao to create a new user
	 * @param user is the input user that need to insert
	 * return dao.create
	 */
	@Override
	public boolean create(User user) {
		// TODO Auto-generated method stub
		logger.info("Entering create user");
		return Dao.create(user);
	}

	
	/*
	 * (non-Javadoc)
	 * @see business.BusinessInterface#findUser(java.lang.String)
	 * business function to check if  a user by username is in database
	 * @param username is the string username need to find
	 * return boolean
	 */
	@Override
	public boolean findUser(String username) {
		// TODO Auto-generated method stub
		logger.info("find user by username");
		return Dao.findBy(username);
	}

	/*
	 * (non-Javadoc)
	 * @see business.BusinessInterface#findByUser(java.lang.String)
	 * business function to find a username in database and return to user
	 *  @param username is the string username need to find
	 * return user
	 */
	@Override
	public User findByUser(String username) {
		// TODO Auto-generated method stub
		return Dao.findByUser(username);
	}
	

}
