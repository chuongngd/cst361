package business;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import beans.ResponseDataModel;

import beans.Weather;

import util.LoggingInterceptor;
import util.WeatherNotFoundException;

import beans.DataFactory;
import beans.DataInterface;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
/*
 * This class is REST Service that implements the REST API to support the Weather IoT.
 */
@Interceptors(LoggingInterceptor.class)
@RequestScoped
@Path("/weather")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class WeatherRestService {

	Logger logger = LoggerFactory.getLogger(WeatherRestService.class);

	@Inject
	WeatherBusinessInterface service;

	
	/*
	 * rest service post method to save weahter information into database
	 * Save Weatger Data API at /save using HTTP POST.
	 * @param model The Weather Data to save.
     * @return boolean value
	 */
	@POST
	@Path("/save")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public boolean saveTemperature(Weather model) {
		logger.info("Entering WeatherRestService.saveTemperature()");

		return service.create(model);

	}
	
	
	/*
	 * rest service get method to retrieve all weather data in database
	 */
	
	@GET
	@Path("/getjsonAll")
	@Produces(MediaType.APPLICATION_JSON)
	public DataInterface getWeatherJ() throws WeatherNotFoundException {
		logger.info("Entering WeatherRestService.getWeatherAsJson()");
		List<Weather> weathers = new ArrayList<Weather>();
		try {
			weathers = service.getWeather();
			DataFactory responseFactory = new DataFactory();
			DataInterface response = responseFactory.createDTO(1, "OK", weathers);

			return response;
		} catch (Exception e2) {
			DataFactory responseFactory = new DataFactory();
			DataInterface response = responseFactory.createDTO(-1, "Failed", weathers);
			return response;
		}
	}

	
	/*
	 * Get Weather Data in a Date Range at /getjsonRange/{datefrom}/{dateto} using HTTP GET.
	 * rest service get method to retrieve weather information in a range of dates
	 * @param to dateFrom and dateTo is the string of datetime from url
     * @return DataInterface with error code of 1 for no error, -1 if failed
	 * 
	 */
	@GET
	@Path("/getjsonRange/{datefrom}/{dateto}")
	@Produces(MediaType.APPLICATION_JSON)
	public DataInterface getWeatherFromTo(@PathParam("datefrom") String dateFrom, @PathParam("dateto") String dateTo)
			throws WeatherNotFoundException {
		logger.info("Entering WeatherRestService.getWeatherAsJson()");
		List<Weather> weathers = new ArrayList<Weather>();
		try {
			weathers = service.findWeatherByRange(dateFrom, dateTo);
			DataFactory responseFactory = new DataFactory();
			DataInterface response = responseFactory.createDTO(1, "OK", weathers);

			return response;
		} catch (Exception e2) {
			DataFactory responseFactory = new DataFactory();
			DataInterface response = responseFactory.createDTO(-1, "Failed", weathers);
			return response;
		}
	}

}
