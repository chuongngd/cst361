package data;


import java.util.List;

/*
 * This class is the interface class for Data Access Object that implements CRUD methods for WeatherDAO
 */
public interface WeatherDAOInterface <T>{

	    public boolean insert(T model);
	    
	    public List<T> getWeather();
		public List <T> findByDateRange(String from, String to);
}
