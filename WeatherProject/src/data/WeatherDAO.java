package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import beans.Weather;
import util.DatabaseException;
import util.LoggingInterceptor;

/*
 * This class is the Data Acess Object that implements the CRUD methods for the Weather data
 */
@Interceptors(LoggingInterceptor.class)
@Stateless
@Local(WeatherDAOInterface.class)
@LocalBean
public class WeatherDAO implements WeatherDAOInterface<Weather> {

	/*
	 * function to connect database
	 */
	public Connection mysqlConnect() {
		Connection conn = null;
		String url = "jdbc:mysql://localhost:3306/weather";

		try {
			// this checks if the driver exist
			Class.forName("com.mysql.jdbc.Driver");

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("MySQL JDBC Driver isn't install");
			e.printStackTrace();
		}
		try {
			conn = DriverManager.getConnection(url, "root", "root");

		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
		}
		return conn;
	}

	
	/*
	 * (non-Javadoc)
	 * @see data.WeatherDAOInterface#insert(java.lang.Object)
	 * function to insert new weather data
	 *  @param model Weather populated with data to save.
     * @return True if OK else False if Error.
	 * @throws DatabaseException Unchecked exception thrown for all exception.
	 * 
	 */
	@Override
	public boolean insert(Weather model) {
		// TODO Auto-generated method stubConnection conn = mysqlConnect();
		Connection conn = mysqlConnect();
		SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Statement stmt = conn.createStatement();
			String sql = "INSERT INTO WEATHER (temp, humidity, pressure, datetime) VALUES (" + "'"
					+ model.getTemperature() + "'" + ", " + "'" + model.getHumidity() + "'" + ", " + "'"
					+ model.getPressure() + "'" + ", " + "'" + dtFormat.format(new Date()) + "'" + ")";
			stmt.execute(sql);
			return true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("SQL statement error");
			e.printStackTrace();
			throw new DatabaseException(e);
		}
	}

	
	/*
	 * (non-Javadoc)
	 * @see data.WeatherDAOInterface#getWeather()
	 * function to get weather data from database
	 */
	public List<Weather> getWeather() {
		Connection conn = mysqlConnect();
		List<Weather> weathers = new ArrayList<Weather>();
		// SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		try {
			Statement stmt = conn.createStatement();
			String sql = "SELECT * FROM weather.WEATHER ORDER BY weatherID DESC LIMIT 5";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				weathers.add(new Weather(rs.getFloat("TEMP"), rs.getFloat("HUMIDITY"), rs.getFloat("PRESSURE"),
						rs.getDate("DATETIME").toString() + " " + rs.getTime("DATETIME").toString()));
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println("SQL statement error");
			e.printStackTrace();
			throw new DatabaseException(e);
		}
		return weathers;
	}

	
	/*
	 * (non-Javadoc)
	 * @see data.WeatherDAOInterface#findByDateRange(java.lang.String, java.lang.String)
	 * function to find weather data in a range of dates
     * @param from is string of Beginning DateTime to retrieve.
     * @param to is string of End DateTime to retrieve.
     * @return List of Weather populated with data else empty list if not found.
	 * @throws DatabaseException Unchecked exception thrown for all exception.
	 * @throws ParseException when parse string to date type
	 * 
	 */
	public List<Weather> findByDateRange(String from, String to) {
		Connection conn = mysqlConnect();
		List<Weather> weathers = new ArrayList<Weather>();
		// SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String pattern = "MM-dd-yyyy";
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		Date dateFrom = null;
		try {
			dateFrom = formatter.parse(from);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Date dateTo = null;
		try {
			dateTo = formatter.parse(to);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			Statement stmt = conn.createStatement();
			// String sql = "SELECT * FROM weather.WEATHER WHERE DATETIME >=" + "'" +
			// dateFrom + "'" + "AND DATETIME <= " +"'" + dateTo + "'" + "";
			String sql = "SELECT * FROM weather.WEATHER";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
				Date mysql = format2.parse(rs.getDate("DATETIME").toString());			
				if (mysql.after(dateFrom) && mysql.before(dateTo)) {				
					weathers.add(new Weather(rs.getFloat("TEMP"), rs.getFloat("HUMIDITY"), rs.getFloat("PRESSURE"),
							rs.getDate("DATETIME").toString() + " " + rs.getTime("DATETIME").toString()));
				}
			}
			rs.close();
		} catch (SQLException e) {
			System.out.println("SQL statement error");
			e.printStackTrace();
			throw new DatabaseException(e);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
		return weathers;

	}

}
