package data;

import beans.User;

/*
 * This class is the interface class for Data Access Object that implements the CRUD methods for the User
 */
public interface DaoInterface <T>{
	public boolean findBy(T t );
	public boolean findBy(String username);
	public boolean create(T t);
	public User findByUser(String username);

}
