package beans;

import java.util.List;


/*
 * this class represent for data factory to create DTO
 */
public class DataFactory {
	public DataInterface createDTO(int status, String message,List<Weather> data) {
		if(data == null) return null;
		else
		return new ResponseDataModel(status, message,data);
		
	}
}
