package beans;

import java.util.List;

/*
 * this class is interface to implement factory design pattern
 */
public interface DataInterface {
	
	public List<Weather> getData() ;
	public void setDatas(List<Weather> data) ;
}
